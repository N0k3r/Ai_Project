### Reversi using MIN-MAX and ALPHA-BETA Algorithm 
### Projet IA :2019-2020  (ULCO)
### NIDAL ELFENGAOUI


### Fonction d’évaluation
 Cette fonction appartient à la classe IA, alors elle est utilisée par les deux classes MIN-MAX et ALPHA-BETA , cette fonction retourne la différence du score entre 
 le joueur blanc et le noir, aussi qu'elle privilége les cases aux coins du plateau ou sur les cotes en ajoutant un plus au score.
### Les Compteurs de jeu et de temps :
Dans la fonction demandecoup() on incrémente le compteur d’appels de jeu après chaque coup possible, alors aussi le compteur du temps augmente.J'affiche le nombre d'appels pour le joueur et 
le temps nécessaire pour jouer ce coup.
### Test des algorithmes :
Pour tester les algorithmes j’ai joué plusieurs parties en gardant la même profondeur, le résultat change pas alors qu’on changeant la profondeur, j’obtiens des nouveaux résultats 
### MIN-MAX
Minimax contre random : Quand la profondeur augmente la possibilité  que MIN-MAX gagne augmente 
Minimax Contre lui-même|| Profondeur (1) : blanc a gagné
### Alpha-Beta :
Alpha-BETA contre lui-même : Blanc a gagné
Alpha-BETA contre Random Alpha-BETA a gagné
### Pourquoi l’alpha-beta est préférer que MIN-MAX ?
Le principe de AlphaBeta est de tenir à jour deux variables α et β qui contiennent respectivement à chaque moment du développement de l’arbre la valeur minimale, que le joueur peut espérer obtenir pour le coup à jouer étant donné la position où il se trouve et la valeur maximale.

